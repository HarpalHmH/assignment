package com.rakuten.assignment.controller

import com.rakuten.assignment.service.VouchersService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class VouchersController(
    @Autowired val vouchersService: VouchersService
) {

    @GetMapping(value = ["/vendor/{vendorId}/voucher"], produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun getListOfVouchers(@PathVariable vendorId: Long) = vouchersService.getListOfVouchers(vendorId);
}