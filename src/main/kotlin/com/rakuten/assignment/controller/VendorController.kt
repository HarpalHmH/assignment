package com.rakuten.assignment.controller

import com.rakuten.assignment.service.VendorService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class VendorController(
    @Autowired val vendorService: VendorService
) {

    @GetMapping(value = ["/vendors"], produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun getAllVendors() = vendorService.findAll();

    @PostMapping(value = ["/vendor"])
    fun addNewVendor(
        @RequestParam(value = "name") name: String,
        @RequestParam(value = "voucher") voucher: String
    ) = vendorService.addNewVendor(name, voucher)
}