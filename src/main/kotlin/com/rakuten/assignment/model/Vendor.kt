package com.rakuten.assignment.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("vendor")
data class Vendor(
    @Id val id: Long?,
    val name: String
)
