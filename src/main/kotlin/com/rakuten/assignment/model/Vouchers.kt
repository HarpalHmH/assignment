package com.rakuten.assignment.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("vouchers")
data class Vouchers(
    @Id var id: Long?,
    var vendorId: Long,
    var voucher: String
)
