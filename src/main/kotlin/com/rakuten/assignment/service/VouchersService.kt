package com.rakuten.assignment.service

import com.rakuten.assignment.model.Vouchers
import com.rakuten.assignment.repository.VouchersRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class VouchersService(
    @Autowired val vouchersRepository: VouchersRepository
) {
    fun getListOfVouchers(vendorId: Long) = vouchersRepository.getListOfVouchers(vendorId);

    fun save(id: Long, voucher: String): Mono<Vouchers> {
        val vouchers = Vouchers(null, id, voucher)
        return vouchersRepository.save(vouchers)
    }
}