package com.rakuten.assignment.service

import com.rakuten.assignment.model.Vendor
import com.rakuten.assignment.repository.VendorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class VendorService(
    @Autowired val vendorRepository: VendorRepository,
    @Autowired val vouchersService: VouchersService
) {
    fun findAll() = vendorRepository.findAll();

    fun addNewVendor(name: String, voucher: String): Mono<Vendor> {
        val vendor = Vendor(null, name)
        val monoVendor = vendorRepository.save(vendor)
        monoVendor.flatMap { item: Vendor -> item.id?.let { vouchersService.save(it, voucher) } }
        return monoVendor
    }
}