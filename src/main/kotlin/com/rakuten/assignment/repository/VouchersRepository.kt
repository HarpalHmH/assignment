package com.rakuten.assignment.repository

import com.rakuten.assignment.model.Vouchers
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Flux

interface VouchersRepository : ReactiveCrudRepository<Vouchers, Long> {

    @Query("SELECT * FROM vouchers WHERE vendor_id =:vendorId")
    fun getListOfVouchers(vendorId: Long): Flux<Vouchers>
}