package com.rakuten.assignment.repository

import com.rakuten.assignment.model.Vendor
import org.springframework.data.repository.reactive.ReactiveCrudRepository

interface VendorRepository : ReactiveCrudRepository<Vendor, Long> {
}